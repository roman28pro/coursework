package consumerbasket.entities;

public enum Group {
    CHILDREN("Дети", 77, 88, 112, 118, 21, 44, 18, 360, 201, 5, 3, 50),
    ADULT_PEOPLE("Взрослые", 126, 100, 114, 60, 23, 58, 18, 290, 210, 11, 5, 50),
    RETIREES("Пенсионеры", 98, 80, 98, 45, 21, 54, 16, 257, 200, 10, 4, 50);

    private final String name;
    private final double bakery;
    private final double pastaAndCereals;
    private final double potato;
    private final double vegetables;
    private final double fruit;
    private final double sugar;
    private final double meat;
    private final double fish;
    private final double milk;
    private final double eggs;
    private final double butter;
    private final double tea;

    Group(String name, double bakery, double pastaAndCereals, double potato,
          double vegetables, double fruit, double sugar,
          double meat, double fish, double milk, double eggs,
          double butter, double tea) {
        this.name = name;
        this.bakery = bakery;
        this.pastaAndCereals = pastaAndCereals;
        this.potato = potato;
        this.vegetables = vegetables;
        this.fruit = fruit;
        this.sugar = sugar;
        this.meat = meat;
        this.fish = fish;
        this.milk = milk;
        this.eggs = eggs;
        this.butter = butter;
        this.tea = tea;
    }

    public String getName() {
        return name;
    }

    public double getBakery() {
        return bakery;
    }

    public double getPastaAndCereals() {
        return pastaAndCereals;
    }

    public double getPotato() {
        return potato;
    }

    public double getVegetables() {
        return vegetables;
    }

    public double getFruit() {
        return fruit;
    }

    public double getSugar() {
        return sugar;
    }

    public double getMeat() {
        return meat;
    }

    public double getFish() {
        return fish;
    }

    public double getMilk() {
        return milk;
    }

    public double getEggs() {
        return eggs;
    }

    public double getButter() {
        return butter;
    }

    public double getTea() {
        return tea;
    }
}
