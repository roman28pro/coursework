package consumerbasket.entities;

public class BasketPrices {
    private double bakeryPrice;
    private double pastaAndCerealsPrice;
    private double potatoPrice;
    private double vegetablesPrice;
    private double fruitPrice;
    private double sugarPrice;
    private double meatPrice;
    private double fishPrice;
    private double milkPrice;
    private double eggsPrice;
    private double butterPrice;
    private double teaPrice;

    public BasketPrices(double bakeryPrice, double pastaAndCerealsPrice, double potatoPrice, double vegetablesPrice, double fruitPrice, double sugarPrice, double meatPrice, double fishPrice, double milkPrice, double eggsPrice, double butterPrice, double teaPrice) {
        this.bakeryPrice = bakeryPrice;
        this.pastaAndCerealsPrice = pastaAndCerealsPrice;
        this.potatoPrice = potatoPrice;
        this.vegetablesPrice = vegetablesPrice;
        this.fruitPrice = fruitPrice;
        this.sugarPrice = sugarPrice;
        this.meatPrice = meatPrice;
        this.fishPrice = fishPrice;
        this.milkPrice = milkPrice;
        this.eggsPrice = eggsPrice;
        this.butterPrice = butterPrice;
        this.teaPrice = teaPrice;
    }

    public double calculateYearPrice() {
        return bakeryPrice * Person.getGroup().getBakery() +
                pastaAndCerealsPrice * Person.getGroup().getPastaAndCereals() +
                potatoPrice * Person.getGroup().getPotato() +
                vegetablesPrice * Person.getGroup().getVegetables() +
                fruitPrice * Person.getGroup().getFruit() +
                sugarPrice * Person.getGroup().getSugar() +
                meatPrice * Person.getGroup().getMeat() +
                fishPrice * Person.getGroup().getFish() +
                milkPrice * Person.getGroup().getMilk() +
                eggsPrice * Person.getGroup().getEggs() +
                butterPrice * Person.getGroup().getButter() +
                teaPrice * Person.getGroup().getTea();
    }

    public double getBakeryPrice() {
        return bakeryPrice;
    }

    public void setBakeryPrice(double bakeryPrice) {
        this.bakeryPrice = bakeryPrice;
    }

    public double getPastaAndCerealsPrice() {
        return pastaAndCerealsPrice;
    }

    public void setPastaAndCerealsPrice(double pastaAndCerealsPrice) {
        this.pastaAndCerealsPrice = pastaAndCerealsPrice;
    }

    public double getPotatoPrice() {
        return potatoPrice;
    }

    public void setPotatoPrice(double potatoPrice) {
        this.potatoPrice = potatoPrice;
    }

    public double getVegetablesPrice() {
        return vegetablesPrice;
    }

    public void setVegetablesPrice(double vegetablesPrice) {
        this.vegetablesPrice = vegetablesPrice;
    }

    public double getFruitPrice() {
        return fruitPrice;
    }

    public void setFruitPrice(double fruitPrice) {
        this.fruitPrice = fruitPrice;
    }

    public double getSugarPrice() {
        return sugarPrice;
    }

    public void setSugarPrice(double sugarPrice) {
        this.sugarPrice = sugarPrice;
    }

    public double getMeatPrice() {
        return meatPrice;
    }

    public void setMeatPrice(double meatPrice) {
        this.meatPrice = meatPrice;
    }

    public double getFishPrice() {
        return fishPrice;
    }

    public void setFishPrice(double fishPrice) {
        this.fishPrice = fishPrice;
    }

    public double getMilkPrice() {
        return milkPrice;
    }

    public void setMilkPrice(double milkPrice) {
        this.milkPrice = milkPrice;
    }

    public double getEggsPrice() {
        return eggsPrice;
    }

    public void setEggsPrice(double eggsPrice) {
        this.eggsPrice = eggsPrice;
    }

    public double getButterPrice() {
        return butterPrice;
    }

    public void setButterPrice(double butterPrice) {
        this.butterPrice = butterPrice;
    }

    public double getTeaPrice() {
        return teaPrice;
    }

    public void setTeaPrice(double teaPrice) {
        this.teaPrice = teaPrice;
    }
}