package consumerbasket.entities;

public class Person {
    private static String name;
    private static int age;
    private static Group group;

    public static String getName() {
        return name;
    }

    public static void setName(String namePerson) {
        name = namePerson;
    }

    public static int getAge() {
        return age;
    }

    public static void setAge(int agePerson) {
        age = agePerson;
    }

    public static Group getGroup() {
        return group;
    }

    public static void setGroup(Group groupPerson) {
        group = groupPerson;
    }
}
