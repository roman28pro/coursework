package consumerbasket.utils;

import consumerbasket.entities.Group;

public class GroupFactory {
    public static Group getGroup(int age) {
        if (age < 18) {
            return Group.CHILDREN;
        } else if(age < 65) {
            return Group.ADULT_PEOPLE;
        } else {
            return Group.RETIREES;
        }
    }
}
