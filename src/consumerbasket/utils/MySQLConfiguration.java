package consumerbasket.utils;

public interface MySQLConfiguration {
    String URL = "jdbc:mysql://localhost:3308/consumerbasket?serverTimezone=Europe/Moscow&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "";
    String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
}
