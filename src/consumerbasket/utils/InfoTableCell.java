package consumerbasket.utils;

public class InfoTableCell {
    private String name;
    private double adultPeopleValue;
    private double childrenValue;
    private double retirees;

    public InfoTableCell() {
    }

    public InfoTableCell(String name, double adultPeopleValue, double childrenValue, double retirees) {
        this.name = name;
        this.adultPeopleValue = adultPeopleValue;
        this.childrenValue = childrenValue;
        this.retirees = retirees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAdultPeopleValue() {
        return adultPeopleValue;
    }

    public void setAdultPeopleValue(double adultPeopleValue) {
        this.adultPeopleValue = adultPeopleValue;
    }

    public double getChildrenValue() {
        return childrenValue;
    }

    public void setChildrenValue(double childrenValue) {
        this.childrenValue = childrenValue;
    }

    public double getRetirees() {
        return retirees;
    }

    public void setRetirees(double retirees) {
        this.retirees = retirees;
    }
}
