package consumerbasket.controllers;

import consumerbasket.entities.Group;
import consumerbasket.entities.Person;
import consumerbasket.popupwindows.CalcWindow;
import consumerbasket.popupwindows.ErrorWindow;
import consumerbasket.popupwindows.InfoWindow;
import consumerbasket.utils.GroupFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class MainController {
    @FXML
    private TextField nameInput;

    @FXML
    private Button inputButton;

    @FXML
    private TextField ageInput;

    @FXML
    private Button infoButton;

    @FXML
    void inputData(ActionEvent event) {
        Person person = new Person();

        try {

            String name = nameInput.getText();
            int age = Integer.parseInt(ageInput.getText());
            Group group = GroupFactory.getGroup(age);

            person.setName(name);
            person.setAge(age);
            person.setGroup(group);
            new CalcWindow().open();
        } catch (Exception e) {
            ErrorWindow.openWindow("Невалидные данные");
        }
    }

    @FXML
    void openInfoWindow(ActionEvent event) {
        InfoWindow.open();
    }

}
