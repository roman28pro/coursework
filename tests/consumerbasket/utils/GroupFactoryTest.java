package consumerbasket.utils;

import consumerbasket.entities.Group;
import org.junit.Test;

import static org.junit.Assert.*;

public class GroupFactoryTest {

    @Test
    public void getChildrenGroup() {
        Group group = GroupFactory.getGroup(17);
        assertEquals(group, Group.CHILDREN);
    }

    @Test
    public void getAdultGroup() {
        Group group = GroupFactory.getGroup(18);
        assertEquals(group, Group.ADULT_PEOPLE);
    }

    @Test
    public void getRetireesGroup() {
        Group group = GroupFactory.getGroup(70);
        assertEquals(group, Group.RETIREES);
    }

    @Test
    public void getAllGroup() {
        for (int i = 0; i < 18; i++) {
            Group group = GroupFactory.getGroup(i);
            assertEquals(group, Group.CHILDREN);
        }

        for (int i = 18; i < 65; i++) {
            Group group = GroupFactory.getGroup(i);
            assertEquals(group, Group.ADULT_PEOPLE);
        }

        for (int i = 65; i < 100; i++) {
            Group group = GroupFactory.getGroup(i);
            assertEquals(group, Group.RETIREES);
        }
    }
}
